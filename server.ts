const path = require('path'),
  app = require('express')(),
  server = require('http').Server(app),
  io = require('socket.io')(server),
  Bundler = require('parcel')

const entryFile = path.join(__dirname, './web/index.html')
const options = {}
const bundler = new Bundler(entryFile, options)

app.use(bundler.middleware())

io.on('connection', socket => {
  console.log('Connection established')

  socket.on('chat', msg => {
    if (msg === 'Anyone there?') {
      socket.emit('chat', 'Yes. Hi there!')
    } else {
      socket.emit('chat', `echo "${msg}"`)
    }
    console.log('message: ' + msg)
  })
})

server.listen(8080, () => console.log(`Server listening on port ${server.address().port}`))
