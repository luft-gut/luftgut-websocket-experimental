function init() {
  output = document.getElementById('output')
  testSocket()
}

function testSocket() {
  var socket = io.connect()
  socket.on('chat', onMessage)
  socket.on('connect', onConnect)
  socket.on('disconnect', onDisconnect)
  socket.on('connect_error', onError)
  socket.on('reconnect_error', onError)

  statusLabel = document.getElementById('status')
  sendButton = document.getElementById('send')
  inputField = document.getElementById('messageInput')

  function onConnect(evt) {
    statusLabel.innerHTML = 'STATUS: CONNECTED'
    doSend('Anyone there?')
    sendButton.disabled = false
    inputField.disabled = false
  }

  function onDisconnect(evt) {
    statusLabel.innerHTML = 'STATUS: DISCONNECTED'
    sendButton.disabled = true
    inputField.disabled = true
  }

  function onMessage(data) {
    writeToScreen('<span class="response";">RESPONSE: ' + data + '</span>')
  }

  function onError(message) {
    writeToScreen('<span class="error">ERROR: ' + message + '</span>')
  }

  function doSend(message) {
    writeToScreen('<span class="sent";">SENT: ' + message + '</span>')
    socket.emit('chat', message)
  }

  function writeToScreen(message) {
    var pre = document.createElement('p')
    pre.style.wordWrap = 'break-word'
    pre.innerHTML = message
    output.appendChild(pre)
  }

  sendButton.addEventListener('click', event => {
    event.preventDefault()
    sendInputMessage()
  })

  inputField.addEventListener('keydown', event => {
    if (event.keyCode === 13) {
      // Enter pressed
      sendInputMessage()
    }
  })

  function sendInputMessage() {
    const value = inputField.value

    if (value === 'clear') {
      inputField.value = ''
      document.getElementById('output').innerHTML = ''
    } else if (value !== '') {
      doSend(value)
      inputField.value = ''
    }
  }
}

window.addEventListener('load', init, false)
